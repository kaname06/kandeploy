const axios = require('axios')

module.exports = async function(data, text) {
    if (data['slack'] && data['slack'].use && data['slack'].webhook) {
        try {
            let res = await axios.post(data['slack'].webhook, {text: `Project: ${data['name']} => ${text}`})
            console.log('Slack message status:', res.data)
        } catch (error) {
            console.log('Cannot send Alert to slack')
        }
    }
}