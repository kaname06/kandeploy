const sh = require('shelljs')
const Slack = require('./SlackAlert')

module.exports = async function(data) {
    await Slack(data, 'Deployment initiated, current stage: Runtime')
    if(data['pm2-id']) {
        if(data.install) {
            console.log('Installing dependencies')
            try {
                sh.exec('npm i', {silent: true})
                console.log('Dependencies installed successfully')
            } catch (error) {
                console.error('An error ocurred while installing dependencies')
                await Slack(data, 'Error found, stage: Runtime')
                return false
            }
        }
        if(data.build) {
            console.log('Building de app')
            try {
                let bld = sh.exec('npm run build', {silent: true})
                if (bld.stderr != '') {
                    console.log('An error ocurred while app was building')
                    await Slack(data, 'Error found, stage: Runtime')
                    return false
                }
                console.log('Build successfully')
            } catch (error) {
                console.error('An error ocurred while app was building')
                await Slack(data, 'Error found, stage: Runtime')
                return false
            }
        }
        console.log('Restarting the app...')
        try {
            let rstrt = sh.exec(`pm2 restart ${data['pm2-id']}`, {silent: true})
            if (rstrt.stderr != '') {
                console.log('Cannot restart pm2 instance process')
                await Slack(data, 'Error found, stage: Runtime')
                return false
            }
            return true
        } catch (error) {
            console.log('An error ocurred while restarting the app')
            await Slack(data, 'Error found, stage: Runtime')
            return false
        }
    }
    else {
        console.log('Installing dependencies')
        try {
            sh.exec('npm i', {silent: true})
            console.log('Dependencies installed successfully')
        } catch (error) {
            console.error('An error ocurred while installing dependencies')
            await Slack(data, 'Error found, stage: Runtime')
            return false
        }
        if (data.build){
            try {
                let bld = sh.exec('npm run build', {silent: true})
                if (bld.stderr != '') {
                    console.error('An error ocurred while app was building')
                    await Slack(data, 'Error found, stage: Runtime')
                    return false
                }
                console.log('Build successfully')
            } catch (error) {
                console.error('An error ocurred while app was building')
                await Slack(data, 'Error found, stage: Runtime')
                return false
            }
        }
        console.log('Starting the app...')
        try {
            let strt = sh.exec(`pm2 start npm --name ${data.key} -- run ${data['start-command'][data.env]}`, {silent: true})
            if (strt.stderr != '') {
                console.log('The pm2 instance cannot be initiated')
                await Slack(data, 'Error found, stage: Runtime')
                return false
            }
            let pm2list = sh.exec('pm2 jlist', {silent: true})
            const { pm_id } = JSON.parse(pm2list).find(v => v.name === data.key)
            return { pm2_id: pm_id }
        } catch (error) {
            await Slack(data, 'Error found, stage: Runtime')
            return false
        }
    }
}