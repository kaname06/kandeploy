const GenerateNginxHost = require('./generateNginxHost')
const ValidateConfigFile = require('./validateConfigFile')
const RuntimeProcess = require('./runtimeProcess')
const GenerateConfigFile = require('./generateConfigFile')
const Slack = require('./SlackAlert')

const { program } = require('commander')

program.version('0.0.1')

program.option('-p, --path <path>', 'nginx generator system url', 'http://localhost:3000')

program.command('deploy').description('deploy ur project quickly')
    .action(async () => {
        console.log('deploying...')
        const { status, data } = ValidateConfigFile()
        if (!status) { console.error('Invalid configuration params'); process.exit(1) }
        const pm2 = await RuntimeProcess(data)
        if (!pm2) { console.error('Cannot start the app, try again'); process.exit(1) }
        if (data['pm2-id'] && !await GenerateNginxHost(data, program.path)) { console.error('Something went wrong while creating nginx host'); process.exit(1) }
        console.log('Aplication deployed successfully...')
        await Slack(data, `Deployment process finished, with pm2 id: ${pm2.pm2_id}`)
    })

program.command('init').description('generate Kandeploy config file')
    .action(() => {
        GenerateConfigFile()
    })

program.parse(process.argv);
