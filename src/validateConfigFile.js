const fs = require('fs')
const path = require('path')

const MAIN_PATH = process.cwd()
const CONFIG_FILE_PATH = path.join(MAIN_PATH, 'kandep.json')

const removeSpecialChars = (s) => {
    var r=s.toLowerCase();
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/ñ/g),"n");                
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");
    r = r.replace(/[&\/\\#,+()$~%.'":*?!¿<>{}]/g,'');
    r = r.trim().split(" ").join("-")                  
    return r;
}

module.exports = function() {
    if(!fs.existsSync(CONFIG_FILE_PATH)) return {status: null}
    
    let configs = JSON.parse((fs.readFileSync(CONFIG_FILE_PATH, {encoding: 'utf-8'})).replace(/\\"|"(?:\\"|[^"])*"|(\/\/.*|\/\*[\s\S]*?\*\/|\#.*)/g, (m, g) => g ? "" : m))
    console.log(configs)
    if (!configs || 
        !configs.domains || 
        !configs.name ||
        !configs.port ||
        !configs.env ||
        !configs['start-command']) {
            return {status: null}
        }

    configs.key = removeSpecialChars(configs.name) 
    return {status: true, data: configs}
}