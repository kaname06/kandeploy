const axios = require('axios')
const sh = require('shelljs')
const Slack = require('./SlackAlert')

// const NGINX_GENERATOR_PATH = 'http://localhost:3000'

module.exports = async function({ domains, key, port, slack, name }, NGINX_GENERATOR_PATH) {
    console.log('generating...')
    await Slack({slack, name}, 'current stage: Creating nginx server instance')
    const resi = sh.exec("netstat -tupln | grep node | awk '{print $4}'", {silent: true})
    let a = resi.stdout.split(':::').map(v => v.replace('\n', ''))
    a.splice(0,1)
    a = a.map(v => parseInt(v))
    a.sort()
    if (a.some(v => v == port)) {return false}
    const payload = {
        servers: domains,
        key,
        proxyPass: `127.0.0.1:${port}`
    }
    let res = await axios.post(`${NGINX_GENERATOR_PATH}/server`, payload)

    if (!res.data.success || res.status != 200){
        await Slack({slack, name}, 'Error found, stage: Creating nginx server instance')
        return false
    }
    return true
}