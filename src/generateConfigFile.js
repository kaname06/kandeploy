const fs = require('fs')
const path = require('path')

const MAIN_PATH = process.cwd()
const CONFIG_FILE_PATH = path.join(MAIN_PATH, 'kandep.json')

module.exports = function() {
    console.log('Generating config file...')
    if (fs.existsSync(CONFIG_FILE_PATH)) {
        console.log('Config already exist in your project')
        process.exit(1)
    }
    let appdata = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'package.json'), 'utf-8'))
    let content = `
    {
        "name": "${appdata.name}", //this should be your app name
        "pm2-id": null, //this is optional value, is the pm2 process or instance id
        "build": true, //set this on true to execute the build command on your proyect
        "install": true, //set this on true to execute npm install command (when pm2-id field was empty o dont exist, the will run ever)
        "domains": ["test.example.com", "test2.example.com"], //There is the all domain name to host your app on nginx config server
        "env": "development", //the NODE_ENV value to deploy
        "start-command": {
            "development": "dev", //the package command to start de app according the env field value
            "production": "start"
        },
        "slack": {
            "use": false, //set this to true to use slack messages app
            "webhook": "<WEBHOOK URL>"
        },
        "port": 3000 //the port where will run the app
    }
    `
    fs.writeFileSync('kandep.json', content, {encoding: 'utf-8'})
    console.log('Config file Generated Successfully')
    process.exit(0)
}